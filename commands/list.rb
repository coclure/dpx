require_relative 'abstract_command'

class List < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'list'
    @desc = 'List projects'
  end

  def execute
    super

    config_project = @app.project_config(false)

    config_project.each_key do |project_name|
      @app.write(project_name)
    end

  end
end