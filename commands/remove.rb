require_relative 'abstract_command'
require 'open3'
require 'yaml'

class Remove < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'remove'
    @desc = 'Remove project'
  end

  def execute
    super
    project_name = @args[1]
    if !File.exist? File.expand_path @app.config_path
      @app.write ('First execute init command')
      exit (1)
    end
    config = YAML.load_file(@app.config_path)

    if config['project'] == nil
      config['project'] = Hash.new
    end
    config['project'].delete(project_name)

    File.new(@app.config_path, "w")
    File.open(@app.config_path, "r+") do |file|
      file.write(config.to_yaml)
    end
  end
end