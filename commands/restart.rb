require_relative 'abstract_command'
require_relative 'start'
require_relative 'stop'
require 'open3'

class Restart < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'restart'
    @desc = 'Restart a project'
  end

  def execute
    super
    stop_command = Stop.new(@app)
    stop_command.define_args(@args)
    stop_command.execute
    start_command = Start.new(@app)
    start_command.define_args(@args)
    start_command.execute
  end
end