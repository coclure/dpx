require 'optparse'

class AbstractCommand

  attr_reader :name, :desc, :opts, :args, :options

  def initialize (app)
    @app = app
    @name = nil
    @desc = nil
    @opts = [
        [:verbose, '-v', '--verbose', 'Run verbosely'],
        [:help, '-h', '--help', 'Show help'],
    ]
  end

  def execute
    if @options[:help]
      help_opts
      exit (0)
    end
  end

  def help_opts
    @app.write ("Usage of #{@name}")
    @app.write ("Options :")
    @opts.each do |opts|
      @app.write ("\e[32m#{opts[1] + ", " + opts[2]}\e[0m" + " " + opts[3])
    end
  end

  def define_args (args)
    @options = {}
    OptionParser.new do |optsParser|
      opts.each do |opts|
        optsParser.on(opts[1], opts[2], opts[3]) do |v|
          @options[opts[0]] = v
        end
      end
    end.parse!
    @args = ARGV
  end
end