require_relative 'abstract_command'
require 'open3'
require 'yaml'

class Init < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'init'
    @desc = 'Init Dpx'
  end

  def execute
    super
    config = {"project" => nil}

    if File.exist? File.expand_path @app.config_path
      @app.write ('You already have a config file. Delete it? (y/n)')
      res = STDIN.gets.strip
      if res != 'y'
        exit(0)
      end
    end
    File.new(@app.config_path, "w")

    File.open(@app.config_path, "r+") do |file|
      file.write(config.to_yaml)
    end
  end
end