require_relative 'abstract_command'
require 'open3'
require 'yaml'

class Add < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'add'
    @desc = 'Add project'
  end

  def execute
    super

    if !File.exist? File.expand_path @app.config_path
      @app.write ('First execute init command')
      exit (1)
    end
    config = YAML.load_file(@app.config_path)

    @app.write ('Enter project key :')
    project_name = STDIN.gets.strip

    if project_name.match(/\s/)
      throw ('Project name must not contains white space')
    end

    @app.write ('Enter project path :')
    project_path = STDIN.gets.strip
    if config['project'] == nil
      config['project'] = Hash.new
    end
    config['project'][project_name] = {'project_path' => project_path}
    File.open(@app.config_path, "r+") do |file|
      file.write(config.to_yaml)
    end
  end
end