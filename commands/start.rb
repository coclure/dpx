require_relative 'abstract_command'
require 'open3'

class Start < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'start'
    @desc = 'Start a project'
  end

  def execute
    super
    project_name = @args[1]
    config_project = @app.project_config(project_name)
    command = 'docker-compose up -d'
    if config_project['start_script'] != nil
      command = './' + config_project['start_script']
    end
    system("cd #{config_project['project_path']} && #{command}")
  end
end