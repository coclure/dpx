require_relative 'abstract_command'
require 'open3'

class Stop < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'stop'
    @desc = 'Stop a project'
  end

  def execute
    super
    project_name = @args[1]
    config_project = @app.project_config(project_name)
    command = 'docker-compose down'
    if config_project['stop_script'] != nil
      command = './' + config_project['stop_script']
    end
    system("cd #{config_project['project_path']} && #{command}")
  end
end