# Project-docker-compose

## Installation

```
git clone ssh://git@gitlab.com:coclure/dpx.git
```
and execute
```
mkdir -p ~/bin
cd ~/bin
ln -s ~/path/to/dpx .
chmod +x ~/bin/dpx
```

## Configuration

First run 
```
dpx init
```
and for add new project run this (follow instruction)
```
dpx add
```

## Autocompletion
Add this lines to you .bashrc
```
if [ -f /path/to/dpx/bash_completion ]; then
    . /path/to/dpx/bash_completion
fi
```
then run :
```
. ~/.bashrc
```

## Custom configuration

You can define a specific script to start or stop container.
Update ".dpx.yml" in your HOME like this :
```
project:
    foo:
        project_path: "/home/foo/project/bar"
        start_script: "start"
        stop_script: "stop"
```